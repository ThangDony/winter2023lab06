import java.util.Scanner;
public class Jackpot {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Welcome to JACKPOT!");
        
        boolean newGame = true;

        int gamesWon = 0;
        int gamesLost = 0;
        
        while (newGame) {
    
            Board board = new Board();
            boolean gameOver = false;
            int numOfTilesClosed = 0;

            while (!gameOver) {
                System.out.println(board);
                if (board.playATurn()) {
                    gameOver = true;
                } else {
                    numOfTilesClosed += 1;
                }
            }
            
            if (numOfTilesClosed >= 7) {
                System.out.println("You have reached the jackpot and won!");
                gamesWon += 1;
            } else {
                System.out.println("You have not reached the jackpot and lost :(");
                gamesLost += 1;
            }

            System.out.println("Play again? (Y/N)");
            String playAgain = keyboard.nextLine();
            if (!playAgain.toUpperCase().equals("Y")) {
                newGame = false;
                System.out.println("Number of W: " + gamesWon);
                System.out.println("Number of L: " + gamesLost);
            }
        }

        keyboard.close();
    }
    
}
